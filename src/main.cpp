#include <boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/cgs.hpp>
#include <Betaboltz.hpp>
#include <CLI/CLI.hpp>

using namespace dfpe;
using namespace boost::units;
using namespace std;
using namespace CLI;



int main(int argc, char** argv)
{
	
	CLI::App app{"Betaboltz CPU Benchmark"};

    bool relativistic = false;    
    app.add_flag("--relativistic,!--classic", relativistic, "Simulation mode");

	int events = 1;
	app.add_option("-e,--events", events, "Events")->check(PositiveNumber);

	int jobs = 1;
	app.add_option("-j,--jobs", jobs, "Number of threads")->check(PositiveNumber);
	
	int interactions = 1000;
	app.add_option("-i,--interactions", interactions, "Number of interactions per event")->check(PositiveNumber);
	
	double fieldValueDouble = 0.5;
	app.add_option("-f,--field", fieldValueDouble, "Field [kV/cm]");
	        
	try { app.parse(argc, argv); } catch (const CLI::ParseError &e) { return app.exit(e); }
	
    BetaboltzSimple beta;
    
    beta.setNumThreads(jobs);
    
    QtySiMassDensity massDensity(1.66575 * si::milli * cgs::grams / cgs::cubic_centimeter);

    // Here we describe the gas mixture
    GasMixture gas;
    gas.addComponent("Ar",  massDensity);

    beta.enableProcess("e", "Ar",  "Biagi");
    
    PerformanceHandler perfHandler;
    beta.addHandler(perfHandler);
    
    InteractionBulletLimiter limiter(interactions);
    beta.addLimiter(limiter);

    ElectronSpecie electronSpecie;

	QtySiElectricField fieldValue(fieldValueDouble * si::kilo * si::volt / cgs::centimeter);

	VectorC3D<QtySiElectricField> electricField(QtySiElectricField(), QtySiElectricField(), fieldValue);                    

	BaseField* field;
	if (relativistic)
		field = new UniformFieldRelativisticChin(electricField);
	else
		field = new UniformFieldClassic(electricField);


	InfiniteDetector detector(gas, *field);
	beta.setDetector(detector);

	vector<ParticleState> initialStates(jobs);
	int runId = beta.execute(electronSpecie, initialStates, events);

	pair<QtySiTime,QtySiTime> timer = perfHandler.getRunTimerLast();
	
	cout << "runId "   << runId
	     << " time " << timer.first
	     << " ± " << timer.second
	     << " per event | Interactions "
	     << beta.getStatsEfficency() << endl;               
	
	delete field;
}
